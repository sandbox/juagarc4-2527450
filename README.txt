MODULE uif_plus_group v.1.0
======================
version: 1.0 (beta)
name: User Import Framework Plus for Group (support for Group module (not OG))
description: User Import Framework to import users from a CSV file with support for group entities
dependencies: 
 - uif(User Import Framework) - https://www.drupal.org/project/uif
 - group (Group, not OG): https://www.drupal.org/project/group 
developer: Raul Garcia for Publicplan in 01.2015

This module adds the capability to assign users to groups. Unless the group exists will be created too.
The functionality of uif is mantained and this module is only usable in conjuction with module "group" otherweise it does not works properly.

This module reads the field "group" of the CSV file used in uif to import users. 
This field must be in the follow format:

group_type#Group_Name#roles (multiple values are delimited by |. See examples at bottom.)

Description of field:
===================================================================================================
group_type: The internal name of the group type. This will be used to create non existent groups.
Group_Name: The Name of the group to insert the user
roles: Roles for the user in this group. A string with the internal role names delimited by +.
e.g. "group1#Name for the Group#admin_group1+moderator_group1" (without quotes).

If a user belogs to more than one group, use the standard delimiter of the module to delimite values.
e.g. "group_type1#Name for the Group#admin_group1+moderator_group1 | group_type1#Name for the Group2#admin_group2+moderator_group2" (without quotes).

The users are updated if they exist, the are not duplicated.


Warnings:
===================================================================================================
- The group name must be double cheked before importing. The groups are always created if the given name are not found. 
It can produce duplicity of groups.
- The format of field "group" must be respected in order to give the right functionality of the module.
-

EXAMPLES:
==================================================================================================
Some examples of formats in CSV file:

The first linie indicates the name of the fields to fill

Simple example: Users belong only to one group and have a single role in this group.

mail;name;roles;field_firstname;field_lastname;group
test1@test.com;test2@test.com;Registered user;John;Doe;group_type_1#Group1 of Type1#user
test2@test.com;test2@test.com;Registered user;Jane;Doe;group_type_1#Group2 of Type1#admin
test3@test.com;test2@test.com;Registered user;Admin;User;group_type_2#Group1 of Type2#admin
test5@test.com;test2@test.com;Registered user;User;Test;group_type_2#Group1 of Type2#user
test4@test.com;test2@test.com;Registered user;John;Smith;group_type_3#Group1 of Type3#user

Advanced exemple: Users belong to more thanone group but have only one role in each group

mail;name;roles;field_firstname;field_lastname;group
test1@test.com;test2@test.com;Registered user;John;Doe;group_type_1#Group1 of Type1#user | group_type_1#Group2 of Type1#admin | group_type_2#Group1 of Type2#admin
test2@test.com;test2@test.com;Registered user;Jane;Doe;group_type_1#Group2 of Type1#admin | group_type_2#Group1 of Type2#user
test3@test.com;test2@test.com;Registered user;Admin;User;group_type_2#Group1 of Type2#admin
test5@test.com;test2@test.com;Registered user;User;Test;group_type_2#Group1 of Type2#user
test4@test.com;test2@test.com;Registered user;John;Smith;group_type_3#Group1 of Type3#user

More advanced example: Users belog to more groups and have more than one role per group

mail;name;roles;field_firstname;field_lastname;group
test1@test.com;test2@test.com;Registered user;John;Doe;group_type_1#Group1 of Type1#user+moderator | group_type_1#Group2 of Type1#admin+user+moderator | group_type_2#Group1 of Type2#admin
test2@test.com;test2@test.com;Registered user;Jane;Doe;group_type_1#Group2 of Type1#admin+account_manager | group_type_2#Group1 of Type2#user
test3@test.com;test2@test.com;Registered user;Admin;User;group_type_2#Group1 of Type2#admin
test5@test.com;test2@test.com;Registered user;User;Test;group_type_2#Group1 of Type2#user+tester+developer
test4@test.com;test2@test.com;Registered user;John;Smith;group_type_3#Group1 of Type3#user+developer